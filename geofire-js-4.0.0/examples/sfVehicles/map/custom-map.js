angular.module('xpress.map',['ngRoute'])

//declared route
.config(['$routeProvider',function($routeProvider){
	$routeProvider.when('/map',{
		templateUrl: 'map/map-tempalate.html',
		controller : 'MapController'
	})
}])

.controller('MapController',function($scope){

initializeMap();

/* Initializes Google Maps */
function initializeMap() {
  var directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  var chicago = new google.maps.LatLng(41.850033, -87.6500523);
  var mapOptions = { zoom:7, mapTypeId: google.maps.MapTypeId.ROADMAP, center: chicago }
  map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
  directionsDisplay.setMap(map);
  getLocation();
  getAnotherLocation();
  var poss = getLocation().pos;
  // Get the location as a Google Maps latitude-longitude object
  var loc = new google.maps.LatLng(center[0], center[1]);
  console.log("*****"+poss);
    
  /* var loc = new google.maps.LatLng(lat, lng);

  // Create the Google Map
  map = new google.maps.Map(document.getElementById("map-canvas"), {
    center: loc,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });*/
  

  // Create a draggable circle centered on the map
  var circle = new google.maps.Circle({
    strokeColor: "#6D3099",
    strokeOpacity: 0.7,
    strokeWeight: 1,
    fillColor: "#B650FF",
    fillOpacity: 0.35,
    map: map,
    center: loc,
    radius: ((radiusInKm) * 1000),
    draggable: true
  });

  //Update the query's criteria every time the circle is dragged
  var updateCriteria = _.debounce(function() {
    var latLng = circle.getCenter();
    geoQuery.updateCriteria({
      center: [latLng.lat(), latLng.lng()],
      radius: radiusInKm
    });
  }, 10);
  google.maps.event.addListener(circle, "drag", updateCriteria);
}

function getAnotherLocation(){
    var poss;
    navigator.geolocation.getCurrentPosition(function(position) {
             console.log("here8");
           var poss = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            $scope.currentPos = poss;
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
    console.log(poss+"&&&&");

  }


})